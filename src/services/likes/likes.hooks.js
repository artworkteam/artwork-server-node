const { disallow } = require('feathers-hooks-common');
const { hooks: { authenticate } } = require('@feathersjs/authentication');
const { associateCurrentUser, queryWithCurrentUser } = require('feathers-authentication-hooks');

function getEntityService(app, { workId, commentId }) {
  if (workId) return app.service('works');
  else if (commentId) return app.service('comments');
  throw new Error('Invalid ids');
}

function getEntityId({ workId, commentId }) {
  return workId || commentId;
}

async function incrementLikeCounter(ctx) {
  const { app, data } = ctx;

  const entityId = getEntityId(data);
  const service = getEntityService(app, data);
  let entity = await service.get(entityId);
  entity = await service.patch(entityId, { likesCount: entity.likesCount + 1 });
  ctx.dispatch = entity;
  return ctx;
}

async function decrementLikeCounters(ctx) {
  const { app, result } = ctx;

  ctx.dispatch = await Promise.all(result.map(async (data) => {
    const entityId = getEntityId(data);
    const service = getEntityService(app, data);
    let entity = await service.get(entityId);
    entity = await service.patch(entityId, { likesCount: entity.likesCount - 1 });
    return entity;
  }));
  return ctx;
}

async function addLikedNotification(ctx) {
  const { app, result, dispatch } = ctx;
  if (result.workId) {
    await app.service('notifications').create({
      recipientId: dispatch.userId,
      initiatorId: result.userId,
      type: 'Work_liked',
      workId: dispatch.id,
      commentId: null,
    });
  } else {
    await app.service('notifications').create({
      recipientId: dispatch.userId,
      initiatorId: result.userId,
      type: 'Comment_liked',
      workId: dispatch.workId,
      commentId: dispatch.id,
    });
  }
}

module.exports = {
  before: {
    all: [],
    find: [],
    get: [],
    create: [
      authenticate('jwt'),
      associateCurrentUser({ idField: 'id', as: 'userId' }),
      // findEntity,
    ],
    update: [disallow('external')],
    patch: [disallow('external')],
    remove: [
      authenticate('jwt'),
      queryWithCurrentUser({ idField: 'id', as: 'userId' }),
    ],
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [
      incrementLikeCounter,
      addLikedNotification,
    ],
    update: [],
    patch: [],
    remove: [
      decrementLikeCounters,
    ],
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
};
