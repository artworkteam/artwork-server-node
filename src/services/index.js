const works = require('./works/works.service.js');
const users = require('./users/users.service.js');
const likes = require('./likes/likes.service.js');
const comments = require('./comments/comments.service.js');

const notifications = require('./notifications/notifications.service.js');

module.exports = function (app) {
  app.configure(works);
  app.configure(users);
  app.configure(likes);
  app.configure(comments);
  app.configure(notifications);
};
