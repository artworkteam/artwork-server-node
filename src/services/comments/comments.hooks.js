const { authenticate } = require('@feathersjs/authentication').hooks;
const {
  iff, isProvider, discard, preventChanges, disallow,
} = require('feathers-hooks-common');
const { associateCurrentUser, restrictToOwner } = require('feathers-authentication-hooks');
const { NotFound } = require('@feathersjs/errors');

async function validateAndFixReply(ctx) {
  const { app, data } = ctx;
  const { workId, replyId } = data;

  if (!replyId) return ctx;

  const comment = await app.service('comments').get(replyId);
  if (comment.workId !== workId) throw new NotFound('Cannot find replied comment in the work');
  if (comment.replyId) data.replyId = comment.replyId; // Make comments has only 2 levels
  return ctx;
}

async function addCommentedNotification(ctx) {
  const { app, result } = ctx;
  const work = await app.service('works').get(result.workId);
  await app.service('notifications').create({
    recipientId: work.userId,
    initiatorId: result.userId,
    type: 'Work_commented',
    workId: work.id,
    commentId: result.id,
  });
}

module.exports = {
  before: {
    all: [],
    find: [],
    get: [],
    create: [
      authenticate('jwt'),
      discard('id', 'userId', 'likesCount'),
      associateCurrentUser({ idField: 'id', as: 'userId' }),
      validateAndFixReply,
    ],
    update: [disallow('external')],
    patch: [
      authenticate('jwt'),
      restrictToOwner({ idField: 'id', ownerField: 'userId' }),
      iff(
        isProvider('external'),
        preventChanges(
          true,
          'id',
          'workId',
          'userId',
          'replyId',
          'pointX',
          'pointY',
          'likesCount',
        ),
      ),
    ],
    remove: [
      authenticate('jwt'),
      restrictToOwner({ idField: 'id', ownerField: 'userId' }),
    ],
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [addCommentedNotification],
    update: [],
    patch: [],
    remove: [],
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
};
