module.exports = {
  description: 'A service to list works with artworks',
  definitions: {
    works: {
      type: 'object',
      required: ['text'],
      properties: {
        title: {
          type: 'string',
          description: 'The message text',
        },
        description: {
          type: 'string',
          description: 'The id of the user that send the message',
        },
      },
    },
  },
};
