// Initializes the `works` service on path `/works`
const createService = require('feathers-sequelize');
const createModel = require('../../models/works.model');
const hooks = require('./works.hooks');
const docs = require('./works.docs');

module.exports = function (app) {
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    name: 'works',
    Model,
    paginate,
  };

  // Initialize our service with any options it requires
  const work = createService(options);
  // Set docs
  work.docs = docs;

  // Add service
  app.use('/works', work);

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('works');

  service.hooks(hooks);
};
