const { disallow } = require('feathers-hooks-common');
const { hooks: { authenticate } } = require('@feathersjs/authentication');
const {
  iff, isProvider, discard, preventChanges,
} = require('feathers-hooks-common');
const { associateCurrentUser, restrictToOwner } = require('feathers-authentication-hooks');

const includeAssociations = (ctx) => {
  const sequelize = ctx.app.get('sequelizeClient');
  ctx.params.sequelize = {
    raw: false,
    include: [
      {
        model: sequelize.models.users,
        as: 'user',
      },
    ],
  };
  return ctx;
};

module.exports = {
  before: {
    all: [],
    find: [includeAssociations],
    get: [includeAssociations],
    create: [
      authenticate('jwt'),
      discard('id', 'userId', 'likesCount', 'suggestionsCount', 'commentsCount'),
      associateCurrentUser({ idField: 'id', as: 'userId' }),
    ],
    update: [disallow('external')],
    patch: [
      authenticate('jwt'),
      restrictToOwner({ idField: 'id', ownerField: 'userId' }),
      iff(
        isProvider('external'),
        preventChanges(
          true,
          'id',
          'userId',
          'imageId',
          'likesCount',
          'suggestionsCount',
          'commentsCount',
        ),
      ),
    ],
    remove: [authenticate('jwt'), restrictToOwner({ idField: 'id', ownerField: 'userId' })],
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
};
