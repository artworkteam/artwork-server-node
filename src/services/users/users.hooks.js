const { disallow } = require('feathers-hooks-common');

const processSocialProfile = (ctx) => {
  const { data } = ctx;

  if (data.facebook) {
    const { profile } = data.facebook;

    data.displayName = profile.displayName;
    data.profileUrl = profile.profileUrl;
    data.imageUrl = profile.photos[0].value;

    if (profile.emails) {
      data.email = profile.emails[0].value;
    }
  }

  if (data.vkontakte) {
    const { params, profile } = data.vkontakte;

    data.displayName = profile.displayName;
    data.profileUrl = profile.profileUrl;
    data.imageUrl = profile.photos[0].value;

    if (params.email) {
      data.email = params.email;
    }
  }

  return ctx;
};

module.exports = {
  before: {
    all: [],
    find: [],
    get: [],
    create: [processSocialProfile],
    update: [disallow('external')],
    patch: [processSocialProfile],
    remove: [disallow('external')],
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
};
