const { authenticate } = require('@feathersjs/authentication').hooks;
const { disallow } = require('feathers-hooks-common');
const {
  restrictToOwner,
  queryWithCurrentUser,
} = require('feathers-authentication-hooks');

module.exports = {
  before: {
    all: [authenticate('jwt')],
    find: [queryWithCurrentUser({ idField: 'id', as: 'recipientId' })],
    get: [restrictToOwner({ idField: 'id', ownerField: 'recipientId' })],
    create: [disallow('external')],
    update: [disallow('external')],
    patch: [disallow('external')],
    remove: [queryWithCurrentUser({ idField: 'id', as: 'recipientId' })],
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
};
