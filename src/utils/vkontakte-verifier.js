const Debug = require('debug');

const debug = Debug('@feathersjs/authentication-oauth2:verify');

const { Verifier } = require('@feathersjs/authentication-oauth2');

class VkontakteVerifier extends Verifier {
  // it's duplicate of original verify method, but adds "params" argument
  // that we need for reading email of vkontakte profile
  // eslint-disable-next-line consistent-return
  verify(req, accessToken, refreshToken, params, profile, done) {
    debug('Checking credentials');
    const { options } = this;
    const query = {
      [options.idField]: profile.id,
      $limit: 1,
    };
    const data = {
      params,
      profile,
      accessToken,
      refreshToken,
    };
    let existing;

    if (this.service.id === null || this.service.id === undefined) {
      debug('failed: the service.id was not set');
      // eslint-disable-next-line max-len
      return done(new Error('the `id` property must be set on the entity service for authentication'));
    }

    if (req && req[options.entity]) {
      existing = req[options.entity];
    }

    if (!existing && req && req.params && req.params[options.entity]) {
      existing = req.params[options.entity];
    }

    if (existing) {
      // eslint-disable-next-line no-underscore-dangle
      return this._updateEntity(existing, data)
        .then(entity => done(null, entity))
        .catch(error => (error ? done(error) : done(null, error)));
    }

    this.service
      .find({ query })
      // eslint-disable-next-line no-underscore-dangle
      .then(this._normalizeResult)
      // eslint-disable-next-line no-underscore-dangle
      .then(entity => (entity ? this._updateEntity(entity, data) : this._createEntity(data)))
      .then((entity) => {
        const id = entity[this.service.id];
        const payload = { [`${this.options.entity}Id`]: id };
        done(null, entity, payload);
      })
      .catch(error => (error ? done(error) : done(null, error)));
  }
}

module.exports = VkontakteVerifier;
