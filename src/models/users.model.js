// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
const Sequelize = require('sequelize');
const shortid = require('shortid');

const { DataTypes } = Sequelize;

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const users = sequelizeClient.define(
    'users',
    {
      id: {
        type: DataTypes.STRING,
        primaryKey: true,
        defaultValue: () => shortid.generate(),
      },
      displayName: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      profileUrl: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      imageUrl: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      email: {
        type: DataTypes.STRING,
      },
      facebookId: {
        type: DataTypes.STRING,
      },
      vkontakteId: {
        type: DataTypes.INTEGER,
      },
    },
    {
      hooks: {
        beforeCount(options) {
          options.raw = true;
        },
      },
    },
  );

  users.associate = function (models) {
    users.hasMany(models.works, { foreignKey: 'userId' });
    users.hasMany(models.comments, { foreignKey: 'userId' });
    users.hasMany(models.likes, { foreignKey: 'userId' });
  };

  return users;
};
