// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
const Sequelize = require('sequelize');
const shortid = require('shortid');

const { DataTypes } = Sequelize;

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const notifications = sequelizeClient.define(
    'notifications',
    {
      id: {
        type: DataTypes.STRING,
        primaryKey: true,
        defaultValue: () => shortid.generate(),
      },
      recipientId: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      initiatorId: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      type: {
        type: DataTypes.ENUM('Work_liked', 'Comment_liked', 'Work_commented'),
        allowNull: false,
      },
      workId: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      commentId: {
        type: DataTypes.STRING,
        allowNull: true,
      },
    },
    {
      hooks: {
        beforeCount(options) {
          options.raw = true;
        },
      },
    },
  );

  notifications.associate = function (models) { // eslint-disable-line no-unused-vars
    // Define associations here
    // See http://docs.sequelizejs.com/en/latest/docs/associations/
  };

  return notifications;
};
