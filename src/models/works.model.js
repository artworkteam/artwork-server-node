// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
const Sequelize = require('sequelize');
const shortid = require('shortid');

const { DataTypes } = Sequelize;

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const works = sequelizeClient.define(
    'works',
    {
      id: {
        type: DataTypes.STRING,
        primaryKey: true,
        defaultValue: () => shortid.generate(),
      },
      userId: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      imageId: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      description: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      likesCount: {
        type: DataTypes.INTEGER,
        allowNull: false,
        defaultValue: 0,
      },
      suggestionsCount: {
        type: DataTypes.INTEGER,
        allowNull: false,
        defaultValue: 0,
      },
      commentsCount: {
        type: DataTypes.INTEGER,
        allowNull: false,
        defaultValue: 0,
      },
    },
    {
      hooks: {
        beforeCount(options) {
          options.raw = true;
        },
      },
    },
  );

  works.associate = function (models) {
    works.belongsTo(models.users, { foreignKey: 'userId' });

    works.hasMany(models.comments, { foreignKey: 'workId', onDelete: 'CASCADE' });
    works.hasMany(models.likes, { foreignKey: 'workId', onDelete: 'CASCADE' });
  };

  return works;
};
