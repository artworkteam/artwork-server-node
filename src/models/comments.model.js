// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
const Sequelize = require('sequelize');
const shortid = require('shortid');

const { DataTypes } = Sequelize;

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const comments = sequelizeClient.define(
    'comments',
    {
      id: {
        type: DataTypes.STRING,
        primaryKey: true,
        defaultValue: () => shortid.generate(),
      },
      workId: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      userId: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      replyId: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      text: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      pointX: {
        type: DataTypes.DOUBLE,
        allowNull: true,
      },
      pointY: {
        type: DataTypes.DOUBLE,
        allowNull: true,
      },
      likesCount: {
        type: DataTypes.INTEGER,
        allowNull: false,
        defaultValue: 0,
      },
    },
    {
      hooks: {
        beforeCount(options) {
          options.raw = true;
        },
      },
      validate: {
        validPoint() {
          if ((this.workId === null) !== (this.commentId === null)) {
            throw new Error('PointX and pointY must be both null or not null');
          }
        },
      },
    },
  );

  comments.associate = function (models) { // eslint-disable-line no-unused-vars
    comments.belongsTo(models.users, { foreignKey: 'userId' });
    comments.belongsTo(models.works, { foreignKey: 'workId', onDelete: 'CASCADE' });
    comments.belongsTo(models.comments, { foreignKey: 'replyId', onDelete: 'CASCADE' });

    comments.hasMany(models.likes, { foreignKey: 'commentId', onDelete: 'CASCADE' });
    comments.hasMany(models.comments, { foreignKey: 'replyId', onDelete: 'CASCADE' });
  };

  return comments;
};
