const Sequelize = require('sequelize');
const shortid = require('shortid');

const { DataTypes } = Sequelize;

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const likes = sequelizeClient.define(
    'likes',
    {
      id: {
        type: DataTypes.STRING,
        primaryKey: true,
        defaultValue: () => shortid.generate(),
      },
      userId: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      workId: {
        type: DataTypes.STRING,
        allowNull: true,
        defaultValue: null,
      },
      commentId: {
        type: DataTypes.STRING,
        allowNull: true,
        defaultValue: null,
      },
    },
    {
      indexes: [
        {
          name: 'likes_user_work_unq',
          unique: true,
          fields: ['userId', 'workId'],
          where: {
            commentId: null,
          },
        },
        {
          name: 'likes_user_comment_unq',
          unique: true,
          fields: ['userId', 'commentId'],
          where: {
            workId: null,
          },
        },
      ],
      hooks: {
        beforeCount(options) {
          options.raw = true;
        },
      },
      validate: {
        validEntityId() {
          if ((this.workId === null) === (this.commentId === null)) {
            throw new Error('Only one of workId and commentId must br null');
          }
        },
      },
    },
  );

  likes.associate = function (models) { // eslint-disable-line no-unused-vars
    likes.belongsTo(models.users, { foreignKey: 'userId' });
    likes.belongsTo(models.works, { foreignKey: 'workId', onDelete: 'CASCADE' });
    likes.belongsTo(models.comments, { foreignKey: 'commentId', onDelete: 'CASCADE' });
  };

  return likes;
};
