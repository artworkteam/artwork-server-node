module.exports = function (app) {
  if (typeof app.channel !== 'function') {
    // If no real-time functionality has been configured just return
    return;
  }

  app.on('connection', (connection) => {
    // On a new real-time connection, add it to the anonymous channel
    app.channel('all').join(connection);
  });

  app.on('login', (authResult, { connection }) => {
    if (connection) {
      const { user } = connection;
      app.channel(`users/${user.id}`).join(connection);
    }
  });

  // app.publish((msg, ctx) => { // eslint-disable-line no-unused-vars
  //   return app.channel('authenticated');
  // });

  // Here you can also add service specific event publishers
  // e..g the publish the `users` service `created` event to the `admins` channel
  // app.service('users').publish('created', () => app.channel('admins'));

  // With the userid and email organization from above you can easily select involved users
  app.service('notifications').publish(data =>
    app.channel(`users/${data.recipientId}`));
};
